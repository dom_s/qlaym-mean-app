import { QlaymMeanAppPage } from './app.po';

describe('qlaym-mean-app App', () => {
  let page: QlaymMeanAppPage;

  beforeEach(() => {
    page = new QlaymMeanAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
