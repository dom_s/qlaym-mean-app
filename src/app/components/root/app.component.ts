import {Component} from '@angular/core';
import {IVariable} from "../linechart/variable.interface";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']
})
export class AppComponent {
    /**
     * Storage of the title string inside the toolbar.
     */
    public title : string;

    /**
     * Storage for label.
     */
    public dataLabel : string;

    /**
     * Storage for label
     */
    public renderLabel : string;

    /**
     * Storage for the values managed by a selection
     */
    public datasetAmount : number[];

    /**
     * Storage for variable values managed by a selection
     */
    public variables : IVariable[];

    /**
     * Storage for ngModel.
     * Stores which variable was choosed
     */
    public selectedState : string;

    /**
     * Storage for ngModel.
     * Stores which amount of data should be fetched
     */
    public amount : number;

    /**
     * Initialize variables to standard values.
     */
    constructor() {
        this.title = "Visualization of datasets";
        this.dataLabel = "Data:";
        this.renderLabel = "Render:";
        this.datasetAmount = [10, 50, 100, 250, 500, 1000, 2000];
        this.amount = this.datasetAmount[0];
        this.selectedState = "var001";
    }

    /**
     * Gets called from child component when change-event
     * gets fired and emits new variables. Listen to this
     * event and store variables fetched from server into this
     * component.
     */
    public handleVariablesUpdated(variables : any) {
        this.variables = variables;
        this.selectedState = variables[0].var;
    }
}
