import {Component, Input, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';
import {ApiService} from "../../services/api.service";
import {IVariable} from "./variable.interface";
import {ILineChartData} from "./lineChartData.interface";
import {ILineChartColor} from "./lineChartColor.interface";

@Component({
    selector: 'lineChartComponent',
    templateUrl: 'lineChart.component.html',
    styleUrls: ['lineChart.component.css']
})
export class LineChartComponent implements OnChanges{
    /**
     * amount specifies the requested length of data
     */
    @Input() amount : number;

    /**
     * selectedState specifies the dataset which should get fetched
     */
    @Input() selectedState : string;

    /**
     * EventEmitter that emits the new variables fetched from
     * server. AppComponent is listening to that.
     */
    @Output() updateVariables = new EventEmitter();

    /**
     * Stores the maximum amount of data which
     * can come from the sever.
     */
    private maxDataAmount : number;

    /**
     * Stores the pages based on the current settings
     * from user.
     *
     * For example: maxDataAmount = 2000, amount = 500 (User wants to see 500 values)
     * maxDataAmount / amount = 4 (pages)
     */
    public pages : number[];

    /**
     * Stores the actual page
     */
    public page : number;

    /**
     * Stores the fetched variables from server
     */
    public variables : IVariable[];

    /**
     * Storage for the data that gets rendered by ng2-charts and
     * chart.js
     */
    public lineChartData : ILineChartData[];

    /**
     * Storage for the labels which gets rendered
     * at the x-axis.
     */
    public lineChartLabels : string[];

    /**
     * Storage for line chart options
     */
    public lineChartOptions : any;

    /**
     * Storage for a bunch of colors which specify
     * the look of the line-chart
     */
    public lineChartColors : ILineChartColor[];

    /**
     * Stores if the line chart legend should be
     * displayed or not
     */
    public lineChartLegend : boolean;

    /**
     * Specifies the type of chart
     * Here: 'line'
     */
    public lineChartType : string;

    /**
     * Initializes all variables and make the first
     * request against the backend to fetch all variables.
     */
    constructor(private api : ApiService) {
        this.maxDataAmount = 2000;
        this.pages = [1];
        this.page = 1;

        this.lineChartData = [{data : [], label: ""}];
        this.lineChartLabels = [];
        this.lineChartOptions = { responsive: true };
        this.lineChartLegend = true;
        this.lineChartType = 'line';
        this.lineChartColors = [{
                backgroundColor: 'rgba(148,159,177,0.2)',
                borderColor: '#673ab7',
                pointBackgroundColor: 'rgba(148,159,177,1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(148,159,177,0.8)'
            }];

        this.api.fetchVariables()
            .subscribe(variables => {
                this.updateVariables.emit(variables);
                this.GetDataForVariable();
            });
    }

    /**
     * Listen to changes of the Inputs.
     * - handles pagination-feature
     * - fetches new data if needed
     */
    ngOnChanges(changes : SimpleChanges) {
        this.page = 1;
        this.pages = [];

        for(let x=0; x<(this.maxDataAmount/this.amount); x++) {
            this.pages.push(x+1);
        }

        this.GetDataForVariable();
    }

    /**
     * Makes an api-call to fetch a single dataset
     */
    public GetDataForVariable() : void {
        this.GenerateBottomLabels(this.amount, ()=> {
            this.api.fetchOneDataset(this.selectedState, this.amount, this.page)
                .subscribe(data => {
                    this.lineChartData = [{data: data.values, label: this.selectedState}];
                });

        });
    }

    /**
     * According to the rendered data the bottom
     * labels of line-chart will be generated.
     */
    private GenerateBottomLabels(count : number, callback : () => void) : void {
        this.lineChartLabels = [];
        let p = this.page-1;
        let offset = p*this.amount;

        for(let x=0; x<count; x++) {
            let val = offset + x + 1;
            this.lineChartLabels.push('val'+ val);

            if(x === count-1) {
                callback();
            }
        }
    }

    /**
     * Gets the next chunk of data
     */
    public NextPage() : void {
        if(this.page < this.pages[this.pages.length-1]) {
            this.page = this.page + 1;
            this.GetDataForVariable();
        }
    }

    /**
     * Get the previous chunk of data
     */
    public PreviousPage() : void {
        if(this.page > 1) {
            this.page = this.page - 1;
            this.GetDataForVariable();
        }
    }
}
