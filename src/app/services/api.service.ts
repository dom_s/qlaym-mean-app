import { Injectable } from '@angular/core';
import {Http, RequestOptions, Headers, Response} from "@angular/http";
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";

@Injectable()
export class ApiService {
    /**
     * Storage for the baseUrl
     */
    private baseURL : string;

    /**
     * Initializes the baseUrl
     */
    constructor(private http:Http) {
        this.baseURL = 'http://localhost:3000/api';
    }

    /**
     * Add RequestOptions (Headers) to set correct
     * content-type and charset.
     *
     * @returns {RequestOptions}
     */
    private getOptions() : RequestOptions {
        let headers : Headers = new Headers();
        headers.append("content-type", "application/json; charset=utf-8");

        let options : RequestOptions = new RequestOptions({headers : headers});
        options.headers = headers;

        return options;
    }

    /**
     * Get all variables stored inside the mongoDB
     * HTTP GET: http://localhost:3000/api/variables
     *
     * @returns {Observable<R>}
     */
    public fetchVariables() : Observable<any> {
        return this.http.get(this.baseURL + '/variables')
            .map(res => res.json(), this.getOptions());
    }

    /**
     * Get a single dataset specified through variable, limit and page.
     * HTTP GET: http://localhost:3000/api/data/:variable?limit=x&page=y
     *
     * @returns any
     */
    public fetchOneDataset(variable : string, limit : number, page : number) : Observable<any>  {
        return this.http.get(this.baseURL + '/data/' + variable + '?limit=' + limit + '&page=' + page)
            .map(res => res.json(), this.getOptions());
    }
}