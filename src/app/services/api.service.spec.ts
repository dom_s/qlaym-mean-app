import {TestBed, async, inject, getTestBed} from "@angular/core/testing";
import {MockBackend, MockConnection} from "@angular/http/testing";
import {ApiService} from "./api.service";
import {BaseRequestOptions, Http, XHRBackend, HttpModule, Response, ResponseOptions} from "@angular/http";
import {IVariable} from "../components/linechart/variable.interface";

describe("Api Service", () => {
    let mockBackend : MockBackend;
    let baseUrl : string = "http://localhost:3000/api";

    beforeEach(async(()=> {
        TestBed.configureTestingModule({
            providers: [
                ApiService,
                MockBackend,
                BaseRequestOptions,
                {
                    provide: Http,
                    deps: [MockBackend, BaseRequestOptions],
                    useFactory: (backend: XHRBackend, defaultOptions: BaseRequestOptions) => {
                        return new Http(backend, defaultOptions);
                    }
                }
            ],
            imports: [HttpModule]
        });

        mockBackend = getTestBed().get(MockBackend);

    }));

    it("should get all variables", async( ()=> {
       let apiService : ApiService;

       getTestBed().compileComponents().then(() => {
          mockBackend.connections.subscribe(
              (connection : MockConnection) => {
                //Make sure url is correct
                expect(connection.request.url).toEqual(baseUrl + "/variables");

                connection.mockRespond(new Response(
                 new ResponseOptions({
                    body : [{
                            var : "var001"
                        },
                        {
                            var : "var002"
                        },
                        {
                            var : "var003"
                        }]
                 }
             )));
          });

          apiService = getTestBed().get(ApiService);
          expect(apiService).toBeDefined();

          apiService.fetchVariables().subscribe((variables : IVariable[])=> {
              expect(variables).toBeDefined();
              expect(variables.length).toBe(3);
              expect(variables[0]).toEqual({var : "var001"});
              expect(variables[1]).toEqual({var : "var002"});
              expect(variables[2]).toEqual({var : "var003"});

          });
       });
    }));

    it("should get data with given limit", async(inject([ApiService], (apiService) => {
          mockBackend.connections.subscribe(
              (connection : MockConnection) => {
                //Make sure the url is correct
                expect(connection.request.url).toEqual(baseUrl + "/data/var001?limit=1&page=1");

                connection.mockRespond(new Response(
                 new ResponseOptions({
                    body : [{
                        var : "var001",
                        values : [1,2,3,4]
                    }]
                 }
             )));
          });

          apiService.fetchOneDataset("var001",1,1).subscribe((data : [{var : string, values : number[]}] )=> {
              expect(data).toBeDefined();
              expect(data.length).toBe(1);
              expect(data[0].var).toBe("var001");
              expect(data[0].values).toEqual([1,2,3,4]);
          });
       })));
});