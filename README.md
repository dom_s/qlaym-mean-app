# QlaymMeanApp

This project is based on the MEAN stack, so data gets stored, updated and fetched from a MongoDb.
Express is used to handle the API. Angular JS 2 to develop and serve the single page application and Node.js
as Backend.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0-beta.32.3.

## Get started
First of all: `npm i` to install all dependencies.

I presume that all necessary installations like NodeJS and MongoDb are done and you are
able to start a local MongoDb.

Starting MongoDb vary from one OS to another. In my case I am using Windows 10 and follow these steps:
1. MongoDB requires a data directory to store all data. MongoDB’s default data directory path is the absolute path \data\db on the drive from which you start MongoDB.
 Create this folder by running the following command in a Command Prompt: `md \data\db`
 
2. To start MongoDB, run mongod.exe. For example, from the Command Prompt: `"C:\Program Files\MongoDB\Server\3.4\bin\mongod.exe"`

When `npm i` and `starting mongo` is done use following command to build the AngularJS 2 frontend `ng build`.
This can take a second or two...
After that all is good to go, so use the next command `node server` to start the server. At first startup
the program will parse the dataset.csv and stores it to the MongoDb.

Open your browser and go to `http://localhost:3000`

**Cheers,** <br>
**Dominik Schmitz**
