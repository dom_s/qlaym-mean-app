var fs = require('fs');
var csv = require('fast-csv');
var varModel = require('../mongoose/models/varModel');
var dataModel = require('../mongoose/models/dataModel');

/**
 * Storage for the parsed data from csv-file.
 * @type {Array}
 */
var parsedData = [];

/**
 * Indicator if the fristRow was passed or not.
 * @type {boolean}
 */
var firstRow = true;

/**
 * Storage for the csv-stream.
 * @type {"fs".ReadStream}
 */
var stream = fs.createReadStream("./server/csv/dataset.csv");

/**
 * Module which makes functions available. Only import
 * it in other files.
 * @type {{}}
 */
var exports = module.exports = {};

/**
 * checkForData tries to find data inside the varModel and
 * dataModel collections. If data exists in both collections the
 * csv-file won't be parsed again. If one of these two collections
 * is empty or not created the csv-file is parsed and the mongoDb
 * gets filled with parsed data.
 *
 * Example csv-file:
 * var001, var002, var003
 * -1.92,  2.33,   -0.44
 * 2.12,   4.23,   -1
 * 1.89,   -1.2,   -0.98
 *
 * will result in:
 * [{
 *      var : "var001",
 *      values : [-1.92,  2.33,   -0.44]
 * },
 * {
 *      var : "var002",
 *      values : [2.12,   4.23,   -1]
 * }, ...]
 */
checkForData = function() {
    var varModelPromise = new Promise(function(resolve, reject) {
        varModel.find(function(err, res) {
            if(err){
                return reject(err);
            }

            (res.length > 0) ? resolve(true) : resolve(false);
        });
    });

    var dataModelPromise = new Promise(function(resolve, reject) {
        dataModel.find(function(err, res) {
            if(err){
                return reject(err);
            }

            (res.length > 0) ? resolve(true) : resolve(false);
        });
    });

    Promise.all([varModelPromise, dataModelPromise])
        .then(function(values) {
            //console.log(values);
            if(values[0] && values[1]) {
                console.log("MongoDb already filled with data.. skipping");
            } else {
                //clear collections to be sure that there is no data
                //if a needed collection is missing within mongodb I will
                //also drop the other collection and fill it up with new data
                //from csv to guarantee that the data is consistent.
                varModel.remove(function(err) {
                    if(err) {
                        return console.log(err);
                    }

                    dataModel.remove(function(err) {
                        if(err) {
                            return console.log(err);
                        }

                        console.log("cleaned up old collections..");
                        console.log("Filling mongoDb with data...");

                        csv.fromStream(stream, {header:true})
                            .on("data", function(row) {
                                if (firstRow) {
                                    for (var i = 0; i < row.length; i++) {
                                        parsedData.push({var: row[i], values: []});

                                        varModel.create({var: parsedData[i].var}, function (err) {
                                            if (err) {
                                                return console.error(err);
                                            }
                                        });

                                        if (i === row.length - 1)
                                            firstRow = false;
                                    }
                                } else {
                                    for (var j = 0; j < row.length; j++) {
                                        parsedData[j]['values'].push((isNaN(row[j])) ? null : parseFloat(row[j]));
                                    }
                                }
                            })
                            .on("end", function() {
                                for(var x=0; x<parsedData.length; x++) {
                                    dataModel.create({ var : parsedData[x].var, values : parsedData[x].values }, function(err) {
                                        if(err){
                                            return console.error(err);
                                        }
                                    });
                                }

                                console.log("DONE");
                            });
                    })
                });
            }
    })
    .catch(function(errors) {
        console.log(errors);
    })
};

/**
 * Wrapper method which gets called when the
 * server is starting and connection to mongoDb is established.
 */
exports.fillMongoWithData = function() {
    checkForData();
};

return exports;


