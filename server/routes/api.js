const express = require('express');
const router = express.Router();

var varModel = require('../mongoose/models/varModel');
var dataModel = require('../mongoose/models/dataModel');

/* GET api listing. */
router.get('/', function(req, res) {
    res.send('api works');
});

/**
 * Get all variables parsed from the csv
 * Example-Object:
 *
 * [{
 *      _id : mongo.ObjectId
 *      var : 'var001'
 * }, ...]
 *
 * Example request: GET http://localhost/api/variables
 */
router.get('/variables', function(req, res) {
    varModel.find(function(err, variables){
        if(err) {
            return res.send(err);
        }

        res.json(variables);
    });
});

/**
 * Get single row of dataset.csv (parsed) from
 * mongoDb.
 *
 * Example Object:
 *
 * {
 *      _id : mongo.objectId,
 *      var : 'var001',
 *      values : [1, -2.3, -1.92, 3.45, ...] (length == 2000)
 * }
 *
 * Example request: GET http://localhost:3000/api/data/var001?limit=100&page=1
 *
 * The data which is in the response will change based on the given query parameter
 * specified in the request.
 *
 * Query parameters: limit, page
 *
 * The limit defines how much values the response will take at once.
 * The page defines which chunk of data from the whole dataset is in the response.
 *
 * For example: Given amount of values : number[] is 2000 and the limit is set to
 * 1000. Request with query parameter page=1 will respond with the first 1000 values and
 * next request with query parameter page=2 will respond with second 1000 values.
 * If the user tries to query page=3 a BadRequest Error comes up.
 */
router.get('/data/:variable', function(req, res, next) {
    dataModel.findOne({var : req.params.variable}, function(err, data) {
        if(err) {
            return res.send(err);
        }

        if(data != null && data.values != null && data.values.length > 0) {
            if (req.query.limit != null && req.query.page != null) {
                if (req.query.limit > data.values.length || req.query.limit <= 0) {
                    return res.status(400).send({
                        status: 400,
                        error: "Bad Request",
                        description: "Given limit is out of range!"
                    });
                }

                var maxPageCount = data.values.length / parseInt(req.query.limit);
                var maxOffset = parseInt(req.query.limit);
                var minOffset = 0;

                if (req.query.page > 0 && req.query.page <= maxPageCount) {
                    maxOffset = parseInt(req.query.limit) * parseInt(req.query.page);
                    minOffset = maxOffset - parseInt(req.query.limit);
                } else {
                    return res.status(400).send({
                        status: 400,
                        error: "Bad Request",
                        description: "Given page is out of range!"
                    });
                }

                data.values.splice(maxOffset, data.values.length - 1);
                data.values.splice(0, minOffset);
            }
        } else {
            return res.status(400).send({
                status: 400,
                error: "Bad Request",
                description: "No requested dataset found. Maybe " + req.params.variable + " is wrong."
            });
        }

        res.json(data);
    });
});

module.exports = router;