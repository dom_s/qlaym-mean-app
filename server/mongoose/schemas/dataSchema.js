var mongoose = require('mongoose');

/**
 * Mongoose schema:
 * {
 *      var : String (e.g.: "var001"),
 *      values : [Number] (e.g.: [1.34, -1.2, ...]
 * }
 *
 * string variable (var) and dataset (values) are stored together
 * so that request with query parameter can easily find the wanted data.
 *
 * For example: GET http://localhost:3000/api/data/var001?limit=100&page=1
 */
var dataSchema = new mongoose.Schema({
    var : { type : String, required : true},
    values : { type : [Number], required : true}
});

module.exports = dataSchema;