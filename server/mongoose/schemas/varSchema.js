var mongoose = require('mongoose');

/**
 * Mongoose schema:
 * {
 *      var : String (e.g.: "var001")
 * }
 *
 * Fetch this collection to choose and render a single dataset
 * based on this var.
 *
 * For example: GET http://localhost:3000/api/variables
 */
var varSchema = new mongoose.Schema({
    var : { type : String, required : true}
});

module.exports = varSchema;