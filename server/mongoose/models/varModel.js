var mongoose = require('mongoose');
var schema = require('../schemas/varSchema');

/**
 * Mongoose model build up from varSchema.
 */
var varModel = mongoose.model('varModel', schema);
module.exports = varModel;