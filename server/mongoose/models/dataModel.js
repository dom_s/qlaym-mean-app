var mongoose = require('mongoose');
var schema = require('../schemas/dataSchema');

/**
 * Mongoose model build up from dataSchema
 */
var dataModel = mongoose.model('dataModel', schema);
module.exports = dataModel;